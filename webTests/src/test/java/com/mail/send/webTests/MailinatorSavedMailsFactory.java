package com.mail.send.webTests;

import java.util.List;

import org.testng.annotations.Factory;

import com.framework.Credentials;

public class MailinatorSavedMailsFactory {

	@Factory
	public Object[] credentialsFactory(){
		List<String> cre1 = Credentials.readCredentials().get(0);
		List<String> cre2 = Credentials.readCredentials().get(1);
		List<String> cre3 = Credentials.readCredentials().get(2);
		return new Object[]{new MailinatorSavedMailsTest(cre1.get(0), cre1.get(1)),
				new MailinatorSavedMailsTest(cre2.get(0), cre2.get(1)),
				new MailinatorSavedMailsTest(cre3.get(0), cre3.get(1))};
	}
}
