package com.mail.send.webTests;

import org.testng.annotations.Test;

import com.abstrac.test.AbstractTest;

import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

@Features("As user I want to be able to save e-mails")
public class MailinatorSavedMailsTest extends AbstractTest{
	
	private String login;
	private String pass;
	
	public MailinatorSavedMailsTest(String login, String pass){
		this.login = login;
		this.pass = pass;
	}
	
	@Stories("Verify if user can has saved mails")
	@TestCaseId("B.1")
	@Test
	public void verifyIfUserCanSendMail(){
		mainPage.navigaeToLoginPage().login(login, pass).verifyIfUserHasSavedMails();
	}
	
}