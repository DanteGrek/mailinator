package com.abstrac.test;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.framework.BrowserManager;
import com.main.web.object.Mailinator;
import com.pages.MainPage;

public abstract class AbstractTest {

	protected Mailinator mailinator = new Mailinator();
	protected MainPage mainPage;

	@Parameters("browserType")
	@BeforeMethod
	public void beforeMethod(@Optional("Firefox") String browserType){
		mainPage = mailinator.openMilinator(browserType);
	}
	
	@AfterMethod
	public void afterMethod(){
        BrowserManager.getBrowser().quit();
	}
}
