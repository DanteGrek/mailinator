package com.login.webTests;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Factory;

public class MailinatorNegativeLoginFactory {

	private static int loginSize = 0;
	private static int passSize = 4;
	
	@Factory
	public Object[] invalidCredentialsFactory(){
		return new Object[]{
				new MailinatorNegativeLoginTest(getRandomLogin(),getRandomPass()),
				new MailinatorNegativeLoginTest(getRandomLogin(),getRandomPass()),
				new MailinatorNegativeLoginTest(getRandomLogin(),getRandomPass()),
				new MailinatorNegativeLoginTest(getRandomLogin(),getRandomPass())};
		
	}
	
	public static String getRandomLogin(){
		loginSize++;
		return RandomStringUtils.randomAlphabetic(loginSize)+"@mailinator.com";
	}
	
	public static String getRandomPass(){
		passSize++;
		return RandomStringUtils.randomAlphanumeric(passSize);
		
	}
}
