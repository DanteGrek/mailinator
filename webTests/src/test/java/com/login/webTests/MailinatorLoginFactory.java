package com.login.webTests;

import java.util.List;

import org.testng.annotations.Factory;

import com.framework.Credentials;

public class MailinatorLoginFactory {

	@Factory
	public Object[] credentialsFactory(){
		List<String> cre1 = Credentials.readCredentials().get(0);
		List<String> cre2 = Credentials.readCredentials().get(1);
		List<String> cre3 = Credentials.readCredentials().get(2);
		return new Object[]{new MailinatorLoginTest(cre1.get(0), cre1.get(1)),
				new MailinatorLoginTest(cre2.get(0), cre2.get(1)),
				new MailinatorLoginTest(cre3.get(0), cre3.get(1))};
	}
}
