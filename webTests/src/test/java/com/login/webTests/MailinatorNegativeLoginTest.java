package com.login.webTests;

import org.testng.annotations.Test;

import com.abstrac.test.AbstractTest;

import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

@Features("As user I want to be able to login into the system.")
public class MailinatorNegativeLoginTest extends AbstractTest {
	
	private String login;
	private String pass;
	
	public MailinatorNegativeLoginTest(String login, String pass) {
		this.login = login;
		this.pass = pass;
	}

	@Stories("Verify if user cannot login into the system with invalid credentials.")
	@TestCaseId("A.2")
	@Test
	public void verifyIfUserCannotLoginWithInvalidCredentials(){
		mainPage.navigaeToLoginPage().inputCredentialsAndClickLogin(login, pass)
		.verifyIfUserCannotLoginWithInvalidCredentials("invalid username or password");
	}
	
}
