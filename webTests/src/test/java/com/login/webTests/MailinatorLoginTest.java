package com.login.webTests;
import org.testng.annotations.Test;

import com.abstrac.test.AbstractTest;

import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

@Features("As user I want to be able to login into the system.")
public class MailinatorLoginTest extends AbstractTest {

	private String login;
	private String pass;
	
	public MailinatorLoginTest(String login, String pass) {
		this.login = login;
		this.pass = pass;
	}
	
    @TestCaseId("A.1")	
    @Stories("Verify if user with valid credentials can login to the system.")
    @Test
	public void vetifyIfUserCanLoginWithValideCredentials(){
		mainPage.navigaeToLoginPage().login(login, pass).verifyIfLoginNameOnHeaderIsCorrect(login);
	}
}
