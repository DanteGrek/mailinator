package com.framework;

import org.openqa.selenium.WebDriver;

public class BrowserManager {
	
	private static ThreadLocal<Browser> browsers = new ThreadLocal<Browser>(){
		protected Browser initialValue() {
			return new Browser();
		};
	};
	
	public static WebDriver getBrowser(){
		return browsers.get().getDriver();
	}
	
	public static void startBrowser(String browserType){
		browsers.get().setDriverType(browserType);
		browsers.get().startBrowser();
	}
	
	public static void getUrl(String url){
		browsers.get().getUrl(url);
	}

}
