package com.framework;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Credentials {
	
	private static List<List<String>> credentials = new ArrayList<List<String>>();

	public static List<List<String>> readCredentials(){
		BufferedReader bufferReader = null;
		try {
			bufferReader = new BufferedReader(new FileReader(".credentials.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try{
			bufferReader.lines().forEach(s -> parseCredentials(s));
		}finally {
			try {
				bufferReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return credentials;
	}
	
	private static void parseCredentials(String loginAndPass){
		List<String> cred = new ArrayList<String>();
		 String[] list = loginAndPass.split(" ");
		 for(String l : list){
			 if(!l.equals("login:") && !l.equals("pass:") && l.length()>1){
				 cred.add(l);
			 }
		 }
		 if(cred.size() > 0){
		 credentials.add(cred);
		 }
	}
	public static void main(String[] args) {
		System.out.println(readCredentials());
	}
}
