package com.framework;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.uncommons.reportng.HTMLReporter;

import ru.yandex.qatools.allure.annotations.Attachment;

public class Report extends HTMLReporter implements ITestListener {
	
	@Attachment(value = "Screenshot", type = "image/png")
	public byte[] makeScreenshot(String name) {
		return ((TakesScreenshot) BrowserManager.getBrowser()).getScreenshotAs(OutputType.BYTES);
	}

	@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		makeScreenshot(result.getTestName());
		
	}

	@Override
	public void onTestFailure(ITestResult result) {
		makeScreenshot(result.getTestName());
		
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		makeScreenshot(result.getTestName());
		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		
	}
	
	

}
