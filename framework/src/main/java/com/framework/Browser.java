package com.framework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

class Browser {
	
	private WebDriver driver;
	private String browserType;
	
	public void setDriverType(String type){
		browserType = type;
	}
	
	public WebDriver startBrowser(){
		switch (browserType) {
		case "Chrome":
			return startChrome();
		default:
			return startFireFox();
		}
	}
	
	/**
	 * Start FireFox browser and open maximize the window
	 * @return instants of FireFox
	 */
	private WebDriver startFireFox(){
		if(isLinux()){
			System.setProperty("webdriver.gecko.driver",Settings.PATH_TO_GECKODRIVER_FOR_LINUX);	
		}else if(isWindows()){
			System.setProperty("webdriver.gecko.driver",Settings.PATH_TO_GECKOMEDRIVER_FOR_WINDOWS);
		}else if(isMac()){
			System.setProperty("webdriver.gecko.driver",Settings.PATH_TO_GECKOMEDRIVER_FOR_MAC);
		}
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		return driver;
	}
	
	private WebDriver startChrome(){
		if(isLinux()){
			System.setProperty("webdriver.chrome.driver",Settings.PATH_TO_CHROMEDRIVER_FOR_LINUX);	
		}else if(isWindows()){
			System.setProperty("webdriver.chrome.driver",Settings.PATH_TO_CHROMEDRIVER_FOR_WINDOWS);
		}else if(isMac()){
			System.setProperty("webdriver.chrome.driver",Settings.PATH_TO_CHROMEDRIVER_FOR_MAC);
		}
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		return driver;
	}

	public WebDriver getDriver(){
		return driver;
	}

	public void getUrl(String url){
		driver.get(url);
	}
	
	private static boolean isLinux(){
		return System.getProperty("os.name").toLowerCase().contains("nux"); 
	}
	
	private static boolean isWindows(){
		return System.getProperty("os.name").toLowerCase().contains("win"); 
	}
	
	private static boolean isMac(){
		return System.getProperty("os.name").toLowerCase().contains("mac"); 
	}

}
