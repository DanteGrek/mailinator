package com.framework;

import java.io.File;

public class Settings {
	
	public static final String userDir = System.getProperty("user.dir");
	public static final String PATH_TO_CHROMEDRIVER_FOR_LINUX = userDir+File.separator+".."+File.separator+"Drivers"+File.separator+"chromedriver";
	public static final String PATH_TO_CHROMEDRIVER_FOR_WINDOWS = userDir+File.separator+".."+File.separator+"Drivers"+File.separator+"chromedriver.exe";
	public static final String PATH_TO_CHROMEDRIVER_FOR_MAC = userDir+File.separator+".."+File.separator+"Drivers"+File.separator+"chromedriverMac";
	public static final String PATH_TO_GECKODRIVER_FOR_LINUX = userDir+File.separator+".."+File.separator+"Drivers"+File.separator+"geckodriver";
	public static final String PATH_TO_GECKOMEDRIVER_FOR_WINDOWS = userDir+File.separator+".."+File.separator+"Drivers"+File.separator+"geckodriver.exe";
	public static final String PATH_TO_GECKOMEDRIVER_FOR_MAC = userDir+File.separator+".."+File.separator+"Drivers"+File.separator+"geckodriverMac";

}

