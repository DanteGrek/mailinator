package com.abstracts;

import org.openqa.selenium.support.PageFactory;

import com.framework.BrowserManager;

public abstract class AbstractHtmlElement {
	
	public AbstractHtmlElement() {
		PageFactory.initElements(BrowserManager.getBrowser(), this);
	} 
	
}
