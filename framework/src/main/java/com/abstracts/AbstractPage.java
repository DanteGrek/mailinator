package com.abstracts;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.framework.BrowserManager;

import ru.yandex.qatools.allure.annotations.Step;

public abstract class AbstractPage{

	public AbstractPage() {
		PageFactory.initElements(BrowserManager.getBrowser(), this);
		waitUntilPageLoaded();
	}

	protected Boolean waitForTextToBePresentInElement(WebElement element, String text) {
		WebDriverWait wait = new WebDriverWait(BrowserManager.getBrowser(), 10);
		return wait.until(ExpectedConditions.textToBePresentInElement(element, text));
	}
	
	protected WebElement waitForElementBeVisibile(WebElement element) {
		WebDriverWait wait = new WebDriverWait(BrowserManager.getBrowser(), 10);
		return wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	protected WebElement waitForElement(WebElement element) {
		WebDriverWait wait = new WebDriverWait(BrowserManager.getBrowser(), 10);
		return wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	protected List<WebElement> waitForElements(List<WebElement> elements) {
		WebDriverWait wait = new WebDriverWait(BrowserManager.getBrowser(), 10);
		return wait.until(ExpectedConditions.visibilityOfAllElements(elements));

	}

	@Step("Click on {0}")
	protected void click(WebElement element) {
		waitForElement(element).click();
	}
	
	@Step("Click on {1} element from {0}")
	protected void clickOnElementFromList(List<WebElement> elements, int index) {
		waitForElements(elements).get(index).click();
	}
	@Step("Input: {1} into {0}")
	protected void inputText(WebElement element, String text) {
		waitForElement(element).clear();
		element.sendKeys(text);
	}
	
	protected boolean isElementShown(WebElement element){
		return waitForElement(element).isDisplayed();
	}

	protected void waitUntilPageLoaded() {
		@SuppressWarnings("unused")
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) BrowserManager.getBrowser()).executeScript("return jQuery.active")
						.equals("0") && ((JavascriptExecutor) BrowserManager.getBrowser()).executeScript("return document.readyState")
						.equals("complete");
			}
		};
	}
}
