package com.webelements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import com.abstracts.AbstractHtmlElement;

public class Header extends AbstractHtmlElement {
	
	protected final String HEADER_LOCATION = "//header[contains(@id,'header')]";
	
	@FindBys({@FindBy(xpath = HEADER_LOCATION), @FindBy(xpath = "//a[text()='login']")}) private WebElement loginButton;
	
    public WebElement getLoginButton(){
        return loginButton;
    }
}
