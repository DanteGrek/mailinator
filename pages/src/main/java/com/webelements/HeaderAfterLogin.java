package com.webelements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

public final class HeaderAfterLogin extends Header {
	
	@FindBys({@FindBy(xpath = HEADER_LOCATION), @FindBy(xpath = "//a[@style and not (@onclick or @href)]")}) private WebElement loginName;
	
	public WebElement getLoginName(){
		return loginName;
	}

}
