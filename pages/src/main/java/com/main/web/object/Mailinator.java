package com.main.web.object;

import com.framework.BrowserManager;
import com.pages.MainPage;

public class Mailinator {

	public MainPage openMilinator(String browserType){
		BrowserManager.startBrowser(browserType);
		BrowserManager.getUrl("http://www.mailinator.com");
		return new MainPage();
	} 
	
}