package com.pages;

import static org.testng.Assert.*;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.abstracts.AbstractPage;
import com.webelements.HeaderAfterLogin;

public class EmailPage extends AbstractPage {

	@FindBy(css = "#saved_count") public WebElement savedMailsCount;
	
	private HeaderAfterLogin header = new HeaderAfterLogin();
	
	public EmailPage verifyIfLoginNameOnHeaderIsCorrect(String login){
		assertTrue(waitForElement(header.getLoginName()).getText().contains(login), "User cannot login with valid credentials.");
	   return this;
	}
	
	public EmailPage verifyIfUserHasSavedMails(){
		assertTrue(new Integer(waitForElement(savedMailsCount).getText()) > 0,"User has not saved mails.");
		return this;
	}
}
