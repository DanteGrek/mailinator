package com.pages;

import com.abstracts.AbstractPage;
import com.webelements.Header;

public class MainPage extends AbstractPage {

	private Header header = new Header();

	public LoginPage navigaeToLoginPage(){
		click(header.getLoginButton());
		return new LoginPage();
	}
	
}
