package com.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.asserts.SoftAssert;

import com.abstracts.AbstractPage;

/**
 * Created by solomin on 20.10.16.
 */
public class LoginPage extends AbstractPage {

    @FindBy(xpath = "//input[@id='loginEmail']") private WebElement loginField;
    @FindBy(xpath = "//input[@id='loginPassword']") private WebElement passwordField;
    @FindBy(xpath = "//button[@onclick='login();']") private WebElement loginButton;
    @FindBy(css = "#loginError") private WebElement loginErrorMessage;

    public EmailPage login(String login, String pass){
        inputText(loginField, login);
        inputText(passwordField, pass);
        click(loginButton);
        return new EmailPage();
    }
    
    public LoginPage inputCredentialsAndClickLogin(String login, String pass){
    	inputText(loginField, login);
        inputText(passwordField, pass);
        click(loginButton);
    	return this;
    }
    
    public LoginPage verifyIfUserCannotLoginWithInvalidCredentials(String message){
    	waitUntilPageLoaded();
    	SoftAssert soft = new SoftAssert();
    	soft.assertTrue(waitForTextToBePresentInElement(loginErrorMessage, message), "User cannot see message about invalid credentials.\n"
    			+ "Expected: '"+message+"'\n"
    					+ "Actual: '"+loginErrorMessage.getText()+"'");
		soft.assertAll();
		return this;
    }
    
}
